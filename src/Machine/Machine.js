import React from 'react';

import './Machine.css';

const machine = ( props ) => {
    return (
        <div className="Machine">
            <p>Machine: {props.machineName}</p>
            <p>Programs: {props.Program}</p>
        </div>
    )
};

export default machine;