import React, { Component } from 'react';
import './App.css';
import Person from './Machine/Machine';

class App extends Component {
  state = {
    machines: [
      { 
        id: 1, 
        machineName: 'gd1-dev-dp2', 
        programs: [
          {  
            programName: "Predator",
            programVersion: "9.26"
          },
          {  
            programName: "Logger",
            programVersion: "9.27"
          }
        ],
        Recent: false
      },
      { 
        id: 2, 
        machineName: 'gd2-qaa-dp2', 
        programs: [
          {  
            programName: "Predator",
            programVersion: "9.28"
          },
          {  
            programName: "Logger",
            programVersion: "9.29"
          }
        ],
        Recent: false
      }
    ]
  }

  nameChangedHandler = ( event, id ) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    // const person = Object.assign({}, this.state.persons[personIndex]);

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState( {persons: persons} );
  }

  deletePersonHandler = (personIndex) => {
    // const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  }

  togglePersonsHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState( { showPersons: !doesShow } );
  }

  render () {
    const style = {
      backgroundColor: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    };

    
    let machines2 = (
        <div>
          {this.state.machines.map((machine, index) => {
            return <Person
              machineName="dupa"
              Program={machine.programs}
              key={machine.id}
              changed={(event) => this.nameChangedHandler(event, machine.id)} />
          })}
        </div>
      );

      let machines = (
        <div>
          {this.state.machines.map((machine, index) => {
            let dupa = machine.programs.map((program, index) => {
              return <Person
              machineName={machine.machineName}
              Program={program.programName}
              key={program.programVersion}
              />
            })
            return dupa;
          })}
        </div>
      );

    return (
      <div className="App">
        <h1>Hi, I'm a React App</h1>
        <p>This is really working!</p>
        {machines}
      </div>
    );
    // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Does this work now?'));
  }
}

export default App;
